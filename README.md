#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>

int n=0, i=0, j=0, z=0, k=0;
float c=0;
char opcao;

int N()
{
    printf("\nDigite o valor de N (até onde i varia): ");
    scanf("%d",&n);
}

float C()
{
    printf("\nDigite o valor de C: ");
    scanf("%f",&c);
}

float fatorial(int n)
{
    if(n==0||n==1)
        return 1;
    else
        return n*fatorial(n-1);
}

double usif(int n)
{
    if(n==0)
        return 0;
    else
        return 1/fatorial(n) + usif(n-1);
}

double nfsnan(int n)
{
    if(n==0)
        return 0;
    else
        return fatorial(n)/pow(n,n) + nfsnan(n-1);
}

double muansn(int n)
{
    if(n==0)
        return 0;
    else
        return pow(-1,n)/n + muansn(n-1);
}

double damnsn(int n, int c)
{
    if(n==0)
        return 0;
    else
        return pow(c,(-n))/n + damnsn(n-1,c);
}

void leitura()
{
    char ARQUIVO[]="C:\\Users\\fvanj\\Desktop\\CCH\\arc.txt";
    char MODO2[]="r";
    FILE *fd = NULL;

    if((fd=fopen(ARQUIVO,MODO2))==NULL)
    {
        printf("\nERRO");
        exit(1);
    }

    switch(opcao)
    {
    case 'a':
    case 'A':
    {

        printf("\nO valor da série com N=%d é: %.50lf\n",n, usif(n));
        printf("\nA sequência até N=%d é: \n",n);

        double vetor[n];
        for(i=1; i<=n; i++)
        {
            vetor[i]=1/fatorial(i);
            printf("\n%d\t\t%.50lf",i,vetor[i]);

        }

        k=0;
        printf("\n\nDigite 1 para plotar o gráfico com linhas: ");
        printf("\nDigite 2 para plotar o gráfico sem linhas: \n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&k);
        }
        while(k!=1&&k!=2);

        if(k==1)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico.txt");
        }
        else if (k==2)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico2.txt");
        }

        fclose(fd);

        z=0;
        printf("\n1 - Retorna ao menu");
        printf("\n2 - Finaliza o programa\n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            printf("Obrigado por Utilizar o programa\n");
            break;
        }
    }
    break;

    case 'b':
    case 'B':
    {

        printf("\nO valor da série com N=%d é: %.50lf\n",n,nfsnan(n));
        printf("\nA sequência até N=%d é: \n",n);

        double vetor2[n];
        for(i=1; i<=n; i++)
        {
            vetor2[i]=fatorial(i)/pow(i,i);
            printf("\n%d\t-\t%.50lf",i,vetor2[i]);
        }

        k=0;
        printf("\n\nDigite 1 para plotar o gráfico com linhas: ");
        printf("\nDigite 2 para plotar o gráfico sem linhas: \n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&k);
        }
        while(k!=1&&k!=2);

        if(k==1)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico.txt");
        }
        else if (k==2)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico2.txt");
        }

        fclose(fd);

        z=0;
        printf("\n1 - Retorna ao menu");
        printf("\n2 - Finaliza o programa\n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            printf("Obrigado por Utilizar o programa\n");
            break;
        }
    }
    break;

    case 'c':
    case 'C':
    {

        printf("\nO valor da série com N=%d é: %.50lf\n",n,muansn(n));
        printf("\nA sequência até N=%d é: \n",n);

        double vetor3[n];
        for(i=1; i<=n; i++)
        {
            vetor3[i]=pow(-1,i)/i;
            printf("\n%d\t-\t%.50lf",i,vetor3[i]);
        }

        k=0;
        printf("\n\nDigite 1 para plotar o gráfico com linhas: ");
        printf("\nDigite 2 para plotar o gráfico sem linhas: \n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&k);
        }
        while(k!=1&&k!=2);

        if(k==1)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico.txt");
        }
        else if (k==2)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico2.txt");
        }

        fclose(fd);

        z=0;
        printf("\n1 - Retorna ao menu");
        printf("\n2 - Finaliza o programa\n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            printf("Obrigado por Utilizar o programa\n");
            break;
        }
    }
    break;

    case 'd':
    case 'D':
    {

        printf("\nO valor da série com N=%d é: %.50lf\n",n,damnsn(n,c));
        printf("\nA sequência até N=%d é: \n",n);

        double vetor4[n];
        for(i=1; i<=n; i++)
        {
            vetor4[i]=pow(c,(-i))/i;
            printf("\n%d\t-\t%.50lf",i,vetor4[i]);
        }
        k=0;
        printf("\n\nDigite 1 para plotar o gráfico com linhas: ");
        printf("\nDigite 2 para plotar o gráfico sem linhas: \n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&k);
        }
        while(k!=1&&k!=2);

        if(k==1)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico.txt");
        }
        else if (k==2)
        {
            system("C:\\Users\\fvanj\\Desktop\\CCH\\gnuplot\\bin\\gnuplot -p C:\\Users\\fvanj\\Desktop\\CCH\\grafico2.txt");
        }

        fclose(fd);

        z=0;
        printf("\n1 - Retorna ao menu");
        printf("\n2 - Finaliza o programa\n");

        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            printf("Obrigado por Utilizar o programa\n");
            break;
        }
    }
    break;
    }

    fclose(fd);
}

int main()
{
    setlocale(NULL,"");

    char ARQUIVO[]="C:\\Users\\fvanj\\Desktop\\CCH\\arc.txt";
    char MODO[]="w";

    FILE *fd = NULL;

    if((fd=fopen(ARQUIVO,MODO))==NULL)
    {
        printf("\nERRO");
        exit(1);
    }

    printf("BEM-VINDO AO \"SEQUÊNCIAS E SÉRIES PARCIAIS\"\n");
    printf("Aqui possuimos algumas séries com i de 1 até N, sendop este um valor que você escolhe!\n");
    printf("e C é uma consante de sua escolha!\n");
    printf("Caso tenha alguma dúvida, por favor acesse a opção \"informações\"\n");
    printf("Por favor, escolha uma de nossas séries abaixo para começar:\n");
    printf("A - 1/i! \n");
    printf("B - i!/i^i \n");
    printf("C - (-1)^i/i \n");
    printf("D - C^(-i)/i \n");
    printf("I - INFORMAÇÕES\n\n");

    do
    {
        printf("Sua escolha: ");
        fflush(stdin);
        scanf("%c",&opcao);
        fflush(stdin);
    }
    while(opcao!='a'&&opcao!='A'&&
            opcao!='b'&&opcao!='B'&&
            opcao!='c'&&opcao!='C'&&
            opcao!='d'&&opcao!='D'&&
            opcao!='i'&&opcao!='I');

    switch(opcao)
    {
    case 'a':
    case 'A':
    {
        printf("\nVocê escolheu 1/i!\n");
        N();

        double vetor[n];

        for(i=1; i<=n; i++)
        {
            vetor[i]=(1/fatorial(i))*100;
            fprintf(fd,"%d\t-\t%lf\n",i,vetor[i]);
        }

        fclose(fd);

        printf("\n1 - Retorna ao menu");
        printf("\n2 - Leitura dos dados\n");
        z=0;
        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            leitura();
            break;
        }
    }
    break;

    case 'b':
    case 'B':
    {
        printf("\nVocê escolheu i!/i^i\n");
        N();

        double vetor2[n];
        for(i=1; i<=n; i++)
        {
            vetor2[i]=(fatorial(i)/pow(i,i))*100;
            fprintf(fd,"\n%d\t-\t%lf",i,vetor2[i]);
        }

        fclose(fd);

        printf("\n1 - Retorna ao menu");
        printf("\n2 - Leitura dos dados\n");
        z=0;
        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            leitura();
            break;
        }
    }
    break;

    case 'c':
    case 'C':
    {
        printf("\nVocê escolheu (-1)^i/i\n");
        N();

        double vetor3[n];
        for(i=1; i<=n; i++)
        {
            vetor3[i]=(pow(-1,i)/i)*100;
            fprintf(fd,"\n%d\t-\t%lf",i,vetor3[i]);
        }

        fclose(fd);

        printf("\n1 - Retorna ao menu");
        printf("\n2 - Leitura dos dados\n");
        z=0;
        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            leitura();
            break;
        }
    }
    break;

    case 'd':
    case 'D':
    {
        printf("\nVocê escolheu C^(-i)/i\n");
        C();
        N();

        double vetor4[n];
        for(i=1; i<=n; i++)
        {
            vetor4[i]=(pow(c,(-i))/i)*100;
            fprintf(fd,"\n%d\t-\t%lf",i,vetor4[i]);
        }

        fclose(fd);

        printf("\n1 - Retorna ao menu");
        printf("\n2 - Leitura dos dados\n");
        z=0;
        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            leitura();
            break;
        }
    }
    break;

    case 'i':
    case 'I':
    {
        printf("\nVocê escolheu Informações!\n\n");
        printf("FUNÇÕES DO PROGRAMA: Neste programa você pode escolher entre 4 sequências/séries,\n");
        printf("ele retornará os N primeiros valores da sequência e a série parcial de 1 até N \n");
        printf("MODO DE USO: Digite uma opção para escolher a sequência/série de desejo,\n");
        printf("um valor para N e um valor para \'c\' casa haja necessidade.\n");
        printf("OBSERVAÇÕES: Caso queira retornar ao menu, o valor da série obtida anteriormente será perdida no arquivo!\n");
        printf("Atente-se à escala do gráfico para que equivocos não sejam cometido, o eixo das ordenadas está multiplicado por 100\n");

        fclose(fd);

        printf("\n1 - Retorna ao menu");
        printf("\n2 - Finaliza o programa\n");
        z=0;
        do
        {
            printf("Sua escolha: ");
            scanf("%d",&z);
        }
        while(z!=1&&z!=2);

        if(z==1)
        {
            system("cls");
            main();
        }
        else if (z==2)
        {
            printf("Obrigado por Utilizar o programa\n");
            break;
        }
    }
    break;
    }

    fclose(fd);

    return 0;
}
